import json
from exc import *
from flask import request
from collections import OrderedDict
from bisect import bisect_right
from math import floor
import utils
import weaver.client as wclient
import hyperdex.admin, hyperdex.client
import time
import json
import os
import re
from page_module import PageModule
from threading import RLock

filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for', 'be', 'have', 'it', 'with', 'as',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

MAX_CACHED_RESULTS = 1000
MAX_ENTRY_NUM = 10
MIN_DATE = "1980-01-01"
MAX_DATE = "2015-01-01"
BIN_NUM = 30

start_date = utils.str_to_date(MIN_DATE)
end_date   = utils.str_to_date(MAX_DATE)
bin_gap    = int(floor(utils.date_diff(start_date, end_date) / BIN_NUM))

annual_paper_count = {1000: 151, 1001: 56, 1002: 53, 1003: 34, 1004: 40, 1005: 39, 1006: 35, 1007: 52, 1008: 40, 1009: 45, 1010: 34, 1011: 47, 1012: 41, 1013: 14, 1014: 8, 1015: 8, 1016: 24, 1017: 14, 1018: 6, 1019: 7, 1020: 5, 1021: 19, 1022: 7, 1023: 6, 1024: 16, 1025: 16, 1026: 13, 1027: 12, 1028: 7, 1029: 25, 1030: 10, 1031: 12, 1032: 6, 1033: 5, 1034: 10, 1035: 18, 1036: 4, 1037: 11, 1038: 20, 1039: 6, 1040: 17, 1041: 6, 1042: 9, 1043: 10, 1044: 7, 1045: 15, 1046: 9, 1047: 6, 1048: 10, 1049: 7, 1050: 17, 1051: 7, 1052: 3, 1053: 11, 1054: 8, 1055: 9, 1056: 9, 1057: 11, 1058: 10, 1059: 5, 1060: 5, 1061: 8, 1062: 7, 1063: 4, 1064: 10, 1065: 10, 1066: 7, 1067: 7, 1068: 9, 1069: 3, 1070: 3, 1071: 6, 1072: 5, 1073: 7, 1074: 12, 1075: 6, 1076: 5, 1077: 11, 1078: 7, 1079: 7, 1080: 9, 1081: 9, 1082: 9, 1083: 5, 1084: 9, 1085: 12, 1086: 14, 1087: 3, 1088: 8, 1089: 9, 1090: 3, 1091: 11, 1092: 5, 1093: 119, 1094: 3, 1095: 9, 1096: 46, 1097: 12, 1098: 10, 1099: 8, 1100: 9, 1101: 34, 1102: 34, 1103: 38, 1104: 29, 1105: 30, 1106: 36, 1107: 45, 1108: 26, 1109: 30, 1110: 33, 1111: 58, 1112: 39, 1113: 11, 1114: 10, 1115: 14, 1116: 8, 1117: 7, 1118: 4, 1119: 11, 1120: 9, 1121: 6, 1122: 8, 1123: 8, 1124: 6, 1125: 8, 1126: 6, 1127: 7, 1128: 8, 1129: 10, 1130: 11, 1131: 11, 1132: 4, 1133: 6, 1134: 7, 1135: 6, 1136: 9, 1137: 15, 1138: 5, 1139: 7, 1140: 5, 1141: 12, 1142: 9, 1143: 7, 1144: 5, 1145: 6, 1146: 12, 1147: 12, 1148: 6, 1149: 15, 1150: 9, 1151: 11, 1152: 7, 1153: 11, 1154: 5, 1155: 5, 1156: 7, 1157: 4, 1158: 2, 1159: 9, 1160: 10, 1161: 17, 1162: 2, 1163: 7, 1164: 7, 1165: 6, 1166: 6, 1167: 10, 1168: 4, 1169: 5, 1170: 9, 1171: 26, 1172: 9, 1173: 6, 1174: 2, 1175: 10, 1176: 20, 1177: 38, 1178: 4, 1179: 6, 1180: 5, 1181: 9, 1182: 2, 1183: 9, 1184: 7, 1185: 4, 1186: 4, 1187: 3, 1188: 9, 1189: 4, 1190: 6, 1191: 3, 1192: 5, 1193: 4, 1194: 2, 1195: 11, 1196: 14, 1197: 4, 1198: 15, 1199: 8, 1200: 19, 1201: 49, 1202: 51, 1203: 33, 1204: 34, 1205: 27, 1206: 22, 1207: 39, 1208: 41, 1209: 35, 1210: 57, 1211: 56, 1212: 45, 1213: 11, 1214: 10, 1215: 9, 1216: 6, 1217: 3, 1218: 6, 1219: 2, 1220: 11, 1221: 9, 1222: 3, 1223: 10, 1224: 4, 1225: 11, 1226: 5, 1227: 7, 1228: 3, 1229: 6, 1230: 11, 1231: 5, 1232: 5, 1233: 9, 1234: 12, 1235: 5, 1236: 3, 1237: 12, 1238: 2, 1239: 5, 1240: 2, 1241: 7, 1242: 8, 1243: 8, 1244: 5, 1245: 7, 1246: 3, 1247: 4, 1248: 4, 1249: 6, 1250: 6, 1251: 4, 1252: 4, 1253: 4, 1254: 3, 1255: 6, 1256: 4, 1257: 6, 1258: 5, 1259: 3, 1260: 7, 1261: 3, 1262: 2, 1263: 9, 1264: 9, 1265: 14, 1266: 6, 1267: 2, 1268: 5, 1269: 2, 1270: 4, 1271: 5, 1272: 2, 1273: 5, 1274: 9, 1275: 10, 1276: 12, 1277: 8, 1278: 3, 1279: 5, 1280: 8, 1281: 7, 1282: 6, 1283: 7, 1284: 9, 1285: 4, 1286: 4, 1287: 4, 1288: 1, 1289: 17, 1290: 7, 1291: 8, 1292: 7, 1293: 5, 1294: 6, 1295: 8, 1296: 6, 1297: 4, 1298: 11, 1299: 5, 1300: 14, 1301: 46, 1302: 32, 1303: 35, 1304: 32, 1305: 30, 1306: 33, 1307: 23, 1308: 20, 1309: 19, 1310: 17, 1311: 11, 1312: 9, 1313: 7, 1314: 4, 1315: 7, 1316: 9, 1317: 6, 1318: 3, 1319: 6, 1320: 10, 1321: 16, 1322: 3, 1323: 4, 1324: 7, 1325: 6, 1326: 2, 1327: 7, 1328: 4, 1329: 8, 1330: 4, 1331: 8, 1332: 4, 1333: 6, 1334: 9, 1335: 2, 1336: 1, 1337: 2, 1338: 4, 1339: 7, 1340: 5, 1341: 7, 1342: 5, 1343: 8, 1344: 74, 1345: 8, 1346: 7, 1347: 5, 1348: 9, 1349: 2, 1350: 4, 1351: 8, 1352: 5, 1353: 5, 1354: 10, 1355: 7, 1356: 1, 1357: 3, 1358: 10, 1359: 2, 1360: 8, 1361: 7, 1362: 5, 1363: 6, 1364: 6, 1365: 9, 1366: 7, 1367: 9, 1368: 4, 1369: 5, 1370: 4, 1371: 7, 1372: 4, 1373: 3, 1374: 3, 1375: 4, 1376: 5, 1377: 7, 1378: 2, 1379: 4, 1380: 7, 1381: 5, 1382: 2, 1383: 11, 1384: 5, 1385: 10, 1386: 11, 1387: 3, 1388: 6, 1389: 6, 1390: 9, 1391: 6, 1392: 16, 1393: 5, 1394: 5, 1395: 7, 1396: 5, 1397: 8, 1398: 1, 1399: 8, 1400: 17, 1401: 8, 1402: 6, 1403: 8, 1404: 9, 1405: 9, 1406: 6, 1407: 4, 1408: 7, 1409: 10, 1410: 5, 1411: 3, 1412: 6, 1413: 2, 1414: 3, 1415: 9, 1416: 5, 1417: 5, 1418: 4, 1419: 7, 1420: 9, 1421: 6, 1422: 3, 1423: 3, 1424: 9, 1425: 7, 1426: 8, 1427: 8, 1428: 1, 1429: 6, 1430: 8, 1431: 13, 1432: 5, 1433: 17, 1434: 11, 1435: 6, 1436: 6, 1437: 6, 1438: 5, 1439: 8, 1440: 28, 1441: 5, 1442: 31, 1443: 3, 1444: 3, 1445: 12, 1446: 1, 1447: 5, 1448: 7, 1449: 5, 1450: 10, 1451: 4, 1452: 2, 1453: 4, 1454: 6, 1455: 6, 1456: 3, 1457: 6, 1458: 4, 1459: 5, 1460: 3, 1461: 5, 1462: 2, 1463: 4, 1464: 4, 1465: 10, 1466: 5, 1467: 8, 1468: 3, 1469: 10, 1470: 13, 1471: 5, 1472: 1, 1473: 11, 1474: 3, 1475: 3, 1476: 10, 1477: 7, 1478: 4, 1479: 5, 1480: 4, 1481: 7, 1482: 1, 1483: 5, 1484: 3, 1485: 11, 1486: 2, 1487: 6, 1488: 2, 1489: 4, 1490: 3, 1491: 5, 1492: 8, 1493: 6, 1494: 3, 1495: 7, 1496: 5, 1497: 3, 1498: 2, 1499: 4, 1500: 16, 1501: 5, 1502: 2, 1503: 3, 1504: 5, 1505: 8, 1506: 1, 1507: 8, 1508: 6, 1509: 5, 1510: 7, 1511: 5, 1512: 1, 1513: 4, 1514: 1, 1515: 8, 1516: 4, 1517: 6, 1518: 8, 1519: 6, 1520: 12, 1521: 12, 1522: 6, 1523: 8, 1524: 3, 1525: 6, 1526: 8, 1527: 3, 1528: 9, 1529: 1, 1530: 3, 1531: 5, 1532: 3, 1533: 5, 1534: 4, 1535: 5, 1536: 7, 1537: 5, 1538: 5, 1539: 6, 1540: 3, 1541: 14, 1542: 6, 1543: 6, 1544: 8, 1545: 15, 1546: 5, 1547: 7, 1548: 7, 1549: 5, 1550: 6, 1551: 11, 1552: 2, 1553: 7, 1554: 9, 1555: 4, 1556: 4, 1557: 2, 1558: 6, 1559: 4, 1560: 8, 1561: 3, 1562: 7, 1563: 4, 1564: 2, 1565: 5, 1566: 7, 1567: 5, 1568: 2, 1569: 4, 1570: 9, 1571: 7, 1572: 2, 1573: 5, 1574: 4, 1575: 5, 1576: 2, 1577: 3, 1578: 1, 1579: 5, 1580: 2, 1581: 4, 1582: 4, 1583: 7, 1584: 4, 1585: 6, 1586: 4, 1587: 2, 1588: 4, 1589: 1, 1590: 3, 1591: 6, 1592: 4, 1593: 5, 1594: 2, 1595: 8, 1596: 6, 1597: 5, 1598: 3, 1599: 4, 1600: 39, 1601: 4, 1602: 4, 1603: 4, 1604: 3, 1605: 5, 1606: 3, 1607: 4, 1608: 31, 1609: 1, 1610: 4, 1611: 3, 1612: 5, 1613: 5, 1614: 3, 1615: 2, 1616: 3, 1617: 7, 1618: 12, 1619: 4, 1620: 9, 1621: 3, 1622: 5, 1623: 4, 1624: 6, 1625: 1, 1626: 1, 1627: 4, 1628: 4, 1629: 4, 1630: 7, 1631: 4, 1632: 4, 1633: 2, 1634: 71, 1635: 2, 1636: 1, 1637: 2, 1638: 3, 1639: 4, 1641: 3, 1642: 1, 1643: 3, 1644: 1, 1645: 4, 1646: 2, 1647: 10, 1648: 3, 1649: 2, 1650: 13, 1651: 5, 1652: 1, 1653: 6, 1654: 1, 1655: 3, 1656: 4, 1657: 1, 1658: 2, 1659: 4, 1660: 5, 1661: 7, 1662: 6, 1664: 5, 1665: 7, 1666: 2, 1667: 7, 1668: 2, 1669: 3, 1670: 1, 1671: 3, 1672: 1, 1673: 4, 1674: 2, 1675: 5, 1676: 1, 1677: 6, 1678: 2, 1679: 5, 1680: 3, 1681: 4, 1682: 1, 1683: 1, 1684: 6, 1685: 2, 1686: 1, 1687: 2, 1688: 5, 1689: 5, 1690: 2, 1691: 4, 1692: 2, 1693: 4, 1694: 1, 1695: 5, 1696: 4, 1697: 7, 1698: 3, 1699: 12, 1700: 4, 1701: 3, 1702: 2, 1703: 3, 1704: 2, 1705: 4, 1706: 6, 1707: 3, 1708: 2, 1709: 5, 1710: 6, 1711: 8, 1712: 3, 1713: 6, 1714: 1, 1715: 3, 1716: 3, 1717: 5, 1718: 4, 1719: 3, 1720: 5, 1721: 7, 1722: 9, 1723: 3, 1724: 1, 1725: 4, 1726: 23, 1727: 4, 1728: 1, 1729: 5, 1730: 8, 1731: 1, 1732: 2, 1733: 6, 1734: 3, 1735: 2, 1736: 1, 1737: 1, 1738: 7, 1739: 4, 1740: 7, 1741: 4, 1742: 1, 1743: 1, 1744: 9, 1745: 4, 1746: 1, 1747: 2, 1748: 2, 1749: 5, 1750: 9, 1751: 3, 1752: 1, 1753: 1, 1754: 2, 1755: 4, 1756: 3, 1757: 4, 1758: 5, 1759: 1, 1760: 3, 1761: 7, 1762: 3, 1763: 3, 1764: 1, 1765: 4, 1766: 4, 1767: 3, 1769: 87, 1770: 2, 1771: 4, 1772: 4, 1773: 10, 1774: 3, 1775: 2, 1776: 4, 1777: 2, 1778: 2, 1779: 1, 1780: 11, 1781: 2, 1782: 5, 1783: 6, 1784: 3, 1785: 10, 1786: 4, 1787: 7, 1788: 7, 1789: 8, 1790: 2, 1791: 3, 1792: 4, 1793: 5, 1794: 1, 1795: 2, 1796: 1, 1797: 4, 1798: 7, 1799: 4, 1800: 13, 1801: 14, 1802: 1, 1803: 8, 1804: 1, 1805: 6, 1806: 3, 1807: 7, 1808: 1, 1809: 3, 1810: 3, 1811: 4, 1812: 71, 1813: 5, 1814: 3, 1815: 2, 1816: 6, 1817: 5, 1818: 14, 1819: 5, 1820: 11, 1821: 6, 1822: 5, 1823: 1, 1824: 9, 1825: 13, 1826: 3, 1827: 4, 1828: 3, 1829: 2, 1830: 10, 1831: 2, 1832: 4, 1833: 11, 1834: 1, 1835: 3, 1836: 5, 1837: 5, 1838: 6, 1839: 3, 1840: 2, 1841: 19, 1842: 4, 1843: 5, 1844: 8, 1845: 2, 1846: 6, 1847: 3, 1848: 2, 1849: 1, 1850: 11, 1851: 6, 1852: 7, 1853: 6, 1854: 7, 1855: 1, 1856: 3, 1857: 6, 1858: 8, 1859: 5, 1860: 8, 1861: 6, 1862: 8, 1863: 7, 1864: 16, 1865: 7, 1866: 6, 1867: 12, 1868: 5, 1869: 3, 1870: 7, 1871: 3, 1872: 5, 1873: 6, 1874: 4, 1875: 6, 1876: 8, 1877: 3, 1878: 9, 1879: 6, 1880: 7, 1881: 9, 1882: 13, 1883: 6, 1884: 9, 1885: 12, 1886: 9, 1887: 28, 1888: 13, 1889: 10, 1890: 10, 1891: 12, 1892: 14, 1893: 13, 1894: 7, 1895: 12, 1896: 11, 1897: 16, 1898: 13, 1899: 15, 1900: 56, 1901: 44, 1902: 25, 1903: 25, 1904: 30, 1905: 31, 1906: 42, 1907: 23, 1908: 34, 1909: 29, 1910: 53, 1911: 44, 1912: 27, 1913: 32, 1914: 40, 1915: 38, 1916: 44, 1917: 33, 1918: 46, 1919: 33, 1920: 49, 1921: 33, 1922: 40, 1923: 36, 1924: 31, 1925: 28, 1926: 40, 1927: 46, 1928: 28, 1929: 96, 1930: 99, 1931: 87, 1932: 117, 1933: 107, 1934: 114, 1935: 116, 1936: 135, 1937: 151, 1938: 124, 1939: 146, 1940: 158, 1941: 122, 1942: 122, 1943: 99, 1944: 119, 1945: 110, 1946: 130, 1947: 136, 1948: 176, 1949: 170, 1950: 281, 1951: 244, 1952: 276, 1953: 274, 1954: 285, 1955: 361, 1956: 497, 1957: 494, 1958: 481, 1959: 532, 1960: 522, 1961: 615, 1962: 659, 1963: 698, 1964: 660, 1965: 856, 1966: 1001, 1967: 1108, 1968: 1260, 1969: 1202, 1970: 1229, 1971: 1324, 1972: 1469, 1973: 1663, 1974: 1564, 1975: 1593, 1976: 1681, 1977: 1857, 1978: 2205, 1979: 2348, 1980: 2419, 1981: 2803, 1982: 2986, 1983: 3136, 1984: 3353, 1985: 4025, 1986: 4484, 1987: 5004, 1988: 6031, 1989: 7267, 1990: 9571, 1991: 12237, 1992: 18775, 1993: 27967, 1994: 40313, 1995: 52145, 1996: 67864, 1997: 82194, 1998: 97442, 1999: 108234, 2000: 113811, 2001: 118076, 2002: 131077, 2003: 143093, 2004: 146688, 2005: 151979, 2006: 151067, 2007: 245333, 2008: 433244, 2009: 172385, 2010: 186082, 2011: 182739, 2012: 238392, 2013: 642392, 2014: 478815, 2015: 413439}

bin_paper_count    = [5222, 2986, 3136, 3353, 4025, 4484, 11035, 7267, 9571, 12237, 18775, 27967, 92458, 67864, 82194, 97442, 108234, 113811, 249153, 143093, 146688, 151979, 151067, 678577, 172385, 186082, 182739, 238392, 642392, 932834]

def _setup_space(admin):
    try:
        admin.add_space(' \
            space docs \
            key int doc_id \
            attributes \
                string title, \
                string date, \
                string url \
        ')
        return True
    except:
        return False

def _async_doc_put(client, doc_map):
    return client.async_put('docs', doc_map['id'], {'title':  doc_map['title'].encode('utf-8'),
                                                    'date':   str(doc_map['date']).encode('utf-8'),
                                                    'url':    doc_map['url'].encode('utf-8')})

def _async_doc_get(client, doc_id):
    return client.async_get('docs', doc_id)

def _async_loop(client, ops, num_ops_to_leave):
    done = {}
    while len(ops) > num_ops_to_leave:
        d = client.loop()
        key = ops[d]
        del ops[d]
        done[key] = d.wait()
    return done

def _async_loop_maybe(client, ops):
    return _async_loop(client, ops, 10000)

def _async_loop_all(client, ops):
    return _async_loop(client, ops, 0)

def _get_hd_client(self):
    with self._thread_lock:
        if self._hd_clients:
            return self._hd_clients.pop()
        else:
            return hyperdex.client.Client('128.84.167.220', 7982)

def _done_hd_client(self, c):
    with self._thread_lock:
        self._hd_clients.append(c)

def _get_weaver_client(self):
    with self._thread_lock:
        if self._weaver_clients:
            return self._weaver_clients.pop()
        else:
            return wclient.Client('128.84.167.220', 2002, 'static/yaml/n-gram.yaml')

def _done_weaver_client(self, c):
    with self._thread_lock:
        self._weaver_clients.append(c)

def _get_histogram(self, words):
    global BIN_NUM
    global start_date
    global end_date
    global bin_gap
    global bin_paper_count
    for i in range(len(words)-1):
        if words[i] in filter_list:
            words[i] = '%s_%s' % (words[i], words[i+1][0])
    client = _get_weaver_client(self)
    try:
        ngram_path = client.n_gram_path(words)
    except wclient.WeaverError as e:
        ngram_path = []
    _done_weaver_client(self, client)
    binned_docs = utils.date_histogram(ngram_path, start_date, BIN_NUM, bin_gap)
    assert len(binned_docs) == len(bin_paper_count)
    return binned_docs

def _get_histogram_cached(self, words):
    cache_key = '_'.join(words)
    with self._thread_lock:
        if cache_key in self._cache:
            detailed_res = self._cache[cache_key]
            self._cache[cache_key] = detailed_res
            print('cache hit for {0}'.format(str(words)))
            return detailed_res
    detailed_res = self._get_histogram(words)
    with self._thread_lock:
        if cache_key not in self._cache:
            while len(self._cache) > MAX_CACHED_RESULTS:
                self._cache.popitem(last=False)
            self._cache[cache_key] = detailed_res
    return detailed_res

def _query_to_ngram(self):
    text  = ' '.join(json.loads(request.form['words']))
    words = [x.lower().encode('utf-8') for x in re.findall('\w+', text)]
    return words

def _histogram_handler(self):
    global MIN_DATE
    global MAX_DATE
    global BIN_NUM
    global bin_paper_count
    words = _query_to_ngram(self)
    print('request: {0}'.format(str(words)))
    detailed_res = _get_histogram_cached(self, words)
    hist = []
    for i in range(len(detailed_res)):
        if bin_paper_count[i] > 0:
            hist.append((len(detailed_res[i]) * 100.0)/bin_paper_count[i])
        else:
            hist.append(0.0)
    return {'start_date': MIN_DATE,
            'end_date': MAX_DATE,
            'bin_num': BIN_NUM,
            'hist': hist,
            'paper_count': [len(x) for x in detailed_res]}

def _histogram_bin_handler(self):
    words = _query_to_ngram(self)
    res = _get_histogram_cached(self, words)
    idx = int(request.form['idx'])
    entry_start = int(request.form['entry_start'])
    entry_num = int(request.form['entry_num'])
    if entry_num > MAX_ENTRY_NUM:
        print('too many entries requested')
        return ''

    docs_to_get = res[idx][entry_start:entry_start + entry_num]

    outstanding = {}
    client = _get_hd_client(self)
    for doc in docs_to_get:
        outstanding[_async_doc_get(client, doc[0])] = doc[0]
    docs_got = _async_loop_all(client, outstanding)
    _done_hd_client(self, client)

    doc_list = []
    for doc_id in docs_got:
        doc = docs_got[doc_id]
        doc['id'] = doc_id;
        doc_list.append(doc)

    return doc_list

_n_gram_ajax_handler = {
        'hist': _histogram_handler,
        'bin': _histogram_bin_handler
}

def _n_gram_ajax(self):
    act = request.form['action']
    try:
        res = _n_gram_ajax_handler[act](self)
        return json.dumps(res)
    except KeyError:
        print('invalid action: {0}'.format(act))
        return ''

def _init_doc_map(data_filename, admin, client):
    global BIN_NUM
    global start_date
    global bin_gap
    bins = [i * bin_gap for i in xrange(0, BIN_NUM)]

    print 'init doc map with file', data_filename
    data_file_size = os.path.getsize(data_filename)

    write_hyperdex = _setup_space(admin)
    if write_hyperdex:
        outstanding    = {}

        with open(data_filename, 'r') as f:
            last_progress = 0
            last_time     = time.time()
            first_time    = last_time
            for line in f:
                doc_dict = json.loads(line)
                outstanding[_async_doc_put(client, doc_dict)] = 1
                _async_loop_maybe(client, outstanding)

                progress = float(f.tell())/data_file_size
                if progress > last_progress + 0.01:
                    cur_time      = time.time()
                    print 'progress=%f cur_time=%f total_time=%f' % (progress, cur_time - last_time, cur_time - first_time)
                    last_progress = progress
                    last_time     = cur_time

        _async_loop_all(client, outstanding)

class NGramModule(PageModule):
    _get_histogram = _get_histogram
    _ajax = _n_gram_ajax
    def __init__(self, *args):
        super(NGramModule, self).__init__(*args)
        self._cache          = OrderedDict()
        self._thread_lock    = RLock()
        docs_filename        = self.config_parser.get('n-gram', 'docs_file')
        host                 = self.config_parser.get('n-gram', 'hyperdex_host')
        port                 = self.config_parser.getint('n-gram', 'hyperdex_port')
        self.hyperdex_admin  = hyperdex.admin.Admin(host, port)
        self._hd_clients     = []
        self._weaver_clients = []
        self._n_gram_ajax    = _n_gram_ajax
        _init_doc_map(docs_filename, self.hyperdex_admin, hyperdex.client.Client(host, port))
