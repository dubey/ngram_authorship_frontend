#! /usr/bin/env python
# 
# ===============================================================
#    Description:  Page module class 
# 
#        Created:  2015-11-18 15:49:48
# 
#         Author:  Ayush Dubey, dubey@cs.cornell.edu
# 
# Copyright (C) 2013, Cornell University, see the LICENSE file
#                     for licensing agreement
# ===============================================================
# 

from flask import render_template

class PageModule(object):
    _ajax = False
    def __init__(self, name, title, path, cp, site):
        self.name = name
        self.path = path
        self.title = title
        self.config_parser = cp
        self.site = site
    def gen_app(self):
        if self.path:
            view = lambda: render_template('{0}.html'.format(self.name),
                        site=self.site, current_page=self.name)
        else:
            view = None
        return view, self._ajax
