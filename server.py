from flask import Flask, render_template, request
from ConfigParser import SafeConfigParser
import weaver.client as wclient
import hyperdex.client
from importlib import import_module
import ngram as ngram_module
import authorship as author_module
from page_module import PageModule

config_parser = SafeConfigParser({'weaver': 'True',
                                    'ajax': 'True',
                                    'ajax_method': 'POST',
                                    'static': 'True',
                                    'mod_file': 'True'})
config_parser.read('/var/www/research-trends/server.cfg')

HTTP_HOST = config_parser.get('general', 'host') or '0.0.0.0'
HTTP_PORT = config_parser.getint('general', 'port') or '80'
HTTP_DEBUG = config_parser.getboolean('general', 'debug') or False
BRAND = config_parser.get('general', 'name')

def _get_with_defaults(cp, section, option, default=None):
    if cp.has_option(section, option):
        return cp.get(section, option)
    else:
        return default
SafeConfigParser.get_with_defaults = _get_with_defaults

class Site:
    def __init__(self, pages, brand):
        self.pages = pages
        self.brand = brand

pages = []
site = Site(pages, BRAND)

app = Flask(__name__)

ngram_wc   = wclient.Client('128.84.167.220', 2002, 'static/yaml/n-gram.yaml')
ngram_page = ngram_module.NGramModule('ngram', 'N-gram', '/ngram', config_parser, site)
pages.append(ngram_page)
@app.route('/')
@app.route('/ngram')
def ngram():
    query = request.args.get('query')
    return render_template('n-gram.html', query=query, site=site)

@app.route('/n-gram-query', methods=['POST'])
def ngramajax():
    return ngram_module._n_gram_ajax(ngram_page)

author_wc   = wclient.Client('128.84.167.101', 2002, 'static/yaml/author.yaml')
author_page = author_module.AuthorshipModule('authorship', 'Authors', '/authorship', config_parser, site)
pages.append(author_page)

@app.route('/authorship')
def authorship():
    query = request.args.get('query')
    return render_template('authorship.html', query=query, site=site)

@app.route('/authors-query', methods=['POST'])
def authorajax():
    return author_module._authorship_ajax(author_page)

about_page = PageModule('about', 'About', '/about', config_parser, site)
@app.route('/about')
def about():
    return render_template('about.html', site=site)


if __name__ == "__main__":
    app.run(host=HTTP_HOST, port=HTTP_PORT, threaded=True)
