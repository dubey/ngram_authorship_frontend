from datetime import date
from datetime import timedelta
from time import mktime
from bisect import bisect_right
from math import floor
import re

def str_to_date(string):
    mm = dd = 1
    match = re.match("(\d{4})-(\d{1,2})-(\d{1,2})", string)
    if match:
        dd = int(match.group(3))
    else:
        match = re.match("(\d{4})-(\d{1,2})", string)
    if match:
        mm = int(match.group(2))
    else:
        match = re.match("(\d{4})", string)
    if match:
        yy = int(match.group(1))
    else:
        raise Exception(string)
    return date(yy,mm,dd)

def date_to_int(d):
    return int(mktime(d.timetuple())) * 1000

def date_diff(first, second):
    diff = second - first
    return diff.total_seconds()

def date_histogram(papers, start_date, bin_num, bin_gap):
    # papers is list of tuples (doc_id, doc_date)
    bins = [i * bin_gap for i in xrange(0, bin_num)]
    res = [[] for i in xrange(bin_num)]
    for doc in papers:
        paper_date = str_to_date(doc[1])
        if paper_date < start_date:
            continue
        try:
            idx = bisect_right(bins, date_diff(start_date, paper_date)) - 1
        except:
            print 'date histogram exception with date', doc[1]
            raise
        res[idx].append(doc)
    return res
