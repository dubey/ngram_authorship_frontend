import sys
sys.path.append("../")
from hyperdex_store import *
from ConfigParser import SafeConfigParser
config_parser = SafeConfigParser()
config_parser.read('server.cfg')

node_set = set()
edge_id = 0

host = config_parser.get('paper-repo', 'host') 
port = config_parser.getint('paper-repo', 'port')

a = hyperdex.admin.Admin(host, port)
c = hyperdex.client.Client(host, port)
try:
    cleanup_space(a)
except hyperdex.admin.HyperDexAdminException:
    pass
setup_space(a)

def add_node(word):
    if word not in node_set:
        node_set.add(word)
        sys.stdout.write('<node id="{0}"></node>\n'.format(word))

def add_edge(from_word, to_word, doc_id, doc_pos):
    global edge_id
    sys.stdout.write(('<edge id="{0}" source="{1}" target="{2}">' + \
                    '<data key="doc_id">{3}</data>' + \
                    '<data key="doc_pos">{4}</data></edge>\n') \
        .format(edge_id, from_word, to_word,
                doc_id, doc_pos))
    edge_id += 1

for line in sys.stdin.readlines():
    tokens = [i.strip() for i in line.split()]
    tokens.append("$")
    doc_id = int(tokens[0])
    doc_pos = 0
    title = tokens[2]
    date = tokens[1]
    prev_word = tokens[3]
    add_node(prev_word)
    put_doc(doc_id, DocInfo(doc_id, title, date, 'google.com', ['alice', 'bob']), c)
    for word in tokens[4:]:
        wpair = "{0}_{1}".format(prev_word, word)
        add_node(word)
        add_edge(prev_word, word, doc_id, doc_pos)
        #print wpair, (doc_id, doc_pos)
        prev_word = word
        doc_pos += 1
