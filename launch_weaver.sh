#!/bin/bash
WEAVER_CONF="$1"
WEAVER_RTBASE="$2"
PIDS=()
function run_cmd() {
    "$@" &
    PIDS+=($!)
    sleep 2
}

function shutdown() {
    for pid in "${PIDS[@]}"; do
        kill "$pid"
    done
    $HOME/weaver/startup_scripts/kill_weaver.sh "$WEAVER_CONF"
}

trap shutdown INT
#mkdir -p "$WEAVER_RTBASE"
#./start_weaver.sh "$WEAVER_CONF" "$WEAVER_RTBASE"
$HOME/weaver/startup_scripts/start_weaver.sh "$WEAVER_CONF"
echo "Press ctrl-c to stop"
run_cmd weaver timestamper --config-file="$WEAVER_CONF"
#run_cmd weaver shard --config-file="$WEAVER_CONF"
sleep infinity
